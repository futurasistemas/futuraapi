﻿using AutoMapper;
using ConsultarCNPJ.Models;
using ConsultarCNPJ.Models.ViewModel;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Npgsql;
using RestSharp;
using System.Collections.Generic;
using System.Linq;

namespace ConsultarCNPJ.DAO
{
    public class CnpjDAO
    {
        private IConfiguration _configuracoes;
        private readonly IMapper _mapper;

        public CnpjDAO(IConfiguration config,
            IMapper mapper)
        {
            _configuracoes = config;
            _mapper = mapper;
        }

        public CNPJViewModel RetornaCNPJ(string id)
        {
            using (NpgsqlConnection conexao = new NpgsqlConnection(
                _configuracoes.GetConnectionString("BaseFuturaApi")))
            {
                var cnpjApi = new CNPJViewModel();
                cnpjApi = RetornaCNPJApi(id);
                bool continueBaseFutura = false;
                if(cnpjApi != null)
                {
                    continueBaseFutura = string.IsNullOrEmpty(cnpjApi.error);
                }

                if (continueBaseFutura)
                {
                    var cnpjBaseFutura = conexao.Query<TCNPJ>("Select * From Cnpj Where replace(cnpj,'-','') = @cnpj ", new { cnpj = id.ToString() }).FirstOrDefault();

                    //atualizando a base futura com o cep pesquisado
                    if (cnpjBaseFutura == null)
                    {

                        var maxId = conexao.Query<int?>("Select Max(COALESCE(IdCnpj,0)) as idcep From Cnpj").FirstOrDefault();

                        bool v = maxId == null;
                        int nextId = v || (int)maxId == 0 ? 1 : (int)maxId + 1;

                        cnpjBaseFutura = _mapper.Map<CNPJViewModel,TCNPJ>(cnpjApi);
                        cnpjBaseFutura.idcnpj = nextId;
                        
                        conexao.Insert(cnpjBaseFutura);
                    }
                    else
                    {
                        conexao.Update(cnpjBaseFutura);
                    }


                    if (cnpjApi == null)
                    {
                        cnpjApi = _mapper.Map<TCNPJ, CNPJViewModel>(cnpjBaseFutura);
                    }
                }

                return cnpjApi;
            }
        }

        private CNPJViewModel RetornaCNPJApi(string cnpj)
        {
            var uriClient = _configuracoes["AppSettings:ConsultaCNPJ"];
            var apiReturn = new CNPJViewModel();

            if (!string.IsNullOrEmpty(uriClient))
            {
                RestClient client = new RestClient(string.Format(uriClient, cnpj));
                RestRequest request = new RestRequest(Method.GET);

                //chamada da api 
                IRestResponse<List<string>> response = client.Execute<List<string>>(request);

                try
                {
                    var error = response.Content.Contains("error");
                    if (!error) {
                        apiReturn = JsonConvert.DeserializeObject<CNPJViewModel>(response.Content);
                    }
                    else
                    {
                        apiReturn = JsonConvert.DeserializeObject<CNPJViewModel>(response.Content);
                    }
                        
                }
                catch (System.Exception ex)
                {
                    apiReturn = new CNPJViewModel();
                    apiReturn.error = ex.Message;
                }
            }

            return apiReturn;
        }
    }
}
