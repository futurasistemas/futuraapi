﻿using ConsultarCEP.DAO;
using ConsultarCEP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ConsultarCEP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsultarCepController : ControllerBase
    {
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("{id}")]
        public CEP Get(
            [FromServices] CepDAO cepDAO, int id)
        {
            return cepDAO.RetornaCEP(id);
        }
    }
}
