﻿using ConsultarCNPJ.DAO;
using ConsultarCNPJ.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ConsultarCNPJ.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsultarCNPJController : ControllerBase
    {
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("{id}")]
        public CNPJViewModel Get(
             [FromServices] CnpjDAO cnpjDAO, string id)
        {
            return cnpjDAO.RetornaCNPJ(id);
        }
    }
}
