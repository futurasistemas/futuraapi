﻿using Newtonsoft.Json.Converters;

namespace ConsultarCNPJ.Models.ViewModel.JSONCustomProperty
{
    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter(string format)
        {
            base.DateTimeFormat = format;
        }
    }
}
