﻿using ConsultarCEP.Models;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Npgsql;
using RestSharp;
using System.Collections.Generic;
using System.Linq;

namespace ConsultarCEP.DAO
{
    public class CepDAO
    {
        private IConfiguration _configuracoes;

        public CepDAO(IConfiguration config)
        {
            _configuracoes = config;
        }

        public CEP RetornaCEP(int id)
        {
            using (NpgsqlConnection conexao = new NpgsqlConnection(
                _configuracoes.GetConnectionString("BaseFuturaApi")))
            {
                var cepApi = new CEP();
                cepApi = RetornaCepApi(id);

               
                var cepBaseFutura = conexao.Query<CEP>("Select * From Cep Where replace(cep,'-','') = @cep ", new { cep = id.ToString() }).FirstOrDefault();

                //atualizando a base futura com o cep pesquisado
                if (cepBaseFutura == null)
                {

                    int nextId = conexao.Query<int>("Select Max(COALESCE(IdCep,0)) as idcep From Cep").FirstOrDefault();

                    if(nextId == 0)
                    {
                        nextId = 1;
                    }
                    else
                    {
                        nextId = nextId + 1;
                    }

                    cepApi.idcep = nextId;
                    conexao.Insert(cepApi);
                }
                else
                {
                    conexao.Update(cepApi);
                }


                if(cepApi == null)
                {
                    cepApi = cepBaseFutura;
                }

                return cepApi;
            }
        }

        private CEP RetornaCepApi(int cep)
        {
            var uriClient = _configuracoes["AppSettings:ViaCep"];
            var apiReturn = new CEP();

            if (!string.IsNullOrEmpty(uriClient))
            {
                RestClient client = new RestClient(string.Format(uriClient, cep));
                RestRequest request = new RestRequest(Method.GET);
                
                //chamada da api 
                IRestResponse<List<string>> response = client.Execute<List<string>>(request);

                try
                {
                   apiReturn = JsonConvert.DeserializeObject<CEP>(response.Content);
                }
                catch (System.Exception)
                {
                    apiReturn = new CEP();
                }
            }

            return apiReturn; 
        }


    }
}
