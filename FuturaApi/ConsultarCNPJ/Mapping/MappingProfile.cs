﻿using AutoMapper;
using ConsultarCNPJ.Models;
using ConsultarCNPJ.Models.ViewModel;

namespace ConsultarCNPJ.Mapping
{
    public class MappingProfile : Profile 
    {
        public MappingProfile()
        {
            RegisterDomainToViewModelMapping();
            RegisterViewModelToDomainMapping();
        }

        private void RegisterDomainToViewModelMapping()
        {
            CreateMap<CNPJViewModel, TCNPJ>();
        }

        private void RegisterViewModelToDomainMapping()
        {
            CreateMap<TCNPJ, CNPJViewModel>();
        }
    }
}
