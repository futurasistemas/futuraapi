﻿using ConsultarCNPJ.Models.ViewModel.JSONCustomProperty;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace ConsultarCNPJ.Models.ViewModel
{
    public class CNPJViewModel
    {        
        public int idcnpj { get; set; }
        [JsonProperty("NOME FANTASIA")]
        public string nomefantasia { get; set; }

        [JsonProperty("RAZAO SOCIAL")]
        public string razaosocial { get; set; }

        public string status { get; set; }

        public string cnpj { get; set; }

        [JsonProperty("DATA ABERTURA")]        
        public string dataabertura { get; set; }

        [JsonProperty("CNAE PRINCIPAL DESCRICAO")]
        public string cnae { get; set; }

        [JsonProperty("CNAE PRINCIPAL CODIGO")]
        public string codigocnae { get; set; }

        public string cep { get; set; }

        public string ddd { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }

        [JsonProperty("TIPO LOGRADOURO")]
        public string tipologradouro { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string municipio { get; set; }
        public string uf { get; set; }

        public string error { get; set; }
    }
}
