﻿using Dapper.Contrib.Extensions;
using Newtonsoft.Json;
using System;
using System.ComponentModel;


namespace ConsultarCNPJ.Models
{
    [Table("cnpj")]
    public class TCNPJ
    {
        [ExplicitKey]
        public int idcnpj { get; set; }
        
        public string nomefantasia { get; set; }
        
        public string razaosocial { get; set; }

        public string status { get; set; }

        public string cnpj { get; set; }
        
        public DateTime dataabertura { get; set; }

        public string cnae { get; set; }

        public string codigocnae { get; set; }

        public string cep { get; set; }

        public string ddd { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }

        public string tipologradouro { get; set; }
        public string logradouro { get; set; }
        public string numero { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string municipio { get; set; }
        public string uf { get; set; }

    }
}
