﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace FuturaApi.Repository.Models
{
    public class ApplicationUser : IdentityUser
    { }
}
